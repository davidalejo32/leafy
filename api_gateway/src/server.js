module.exports = {
    users_api_url:
        'https://leafy-users-ms.herokuapp.com',

    publications_api_url:
        'https://leafy-publication-ms.herokuapp.com',

    comments_api_url: 
        'https://leafy-comments-ms.herokuapp.com', 
};
