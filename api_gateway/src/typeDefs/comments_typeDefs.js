const {gql} = require('apollo-server');

const commentsTypeDef = gql`
    type Comments{
        id:Int!
        user_name:String!
        id_publication:Int!
        content:String!
        date:String!
    }

    type getComments{
        id:Int!
        user_name:String!
        id_publication:Int!
        content:String!
        date:String!

    }
    input createComment{
        user_name:String!
        id_publication:Int!
        content:String!
    }

    input updateComment{
        id:Int!
        user_name:String!
        content:String!
    }

    extend type Query{
        comments:[Comments]
        commentByPublicationId(idPublication:Int!):[getComments]
    }

    extend type Mutation{
        createComment(comment:createComment!):Comments!
        updatePublication(comment:updateComment!):String!
        deletePublication(commentId:String!):String!
    }

`

module.exports = commentsTypeDef