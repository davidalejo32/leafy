//call all typeDefs

const users_typeDefs        = require('./users_typeDefs');
const publications_typeDefs = require('./publications_typeDefs');
const comments_typeDef     = require('./comments_typeDefs');

const schemaArrays = [users_typeDefs, publications_typeDefs, comments_typeDef]

module.exports     = schemaArrays
