const {gql} = require('apollo-server');

const publicationsTypeDefs = gql`

    type Publication{
        id:Int!
        user_name:String!
        date:String!
        description:String!
        image:String!
    }

    type PublicationGet{
        id:Int!
        user_name:String!
        date:String!
        description:String!
        image:String!
    }
    
    input publicationCreate{
        email:String!
        description:String!
        image:String
    }

    input publicationUpdate{
        user_name:String!
        description:String!
        image:String!
    }

    extend type Query{
        publications:[Publication]
        publicationByUserName(user_name:String!):[PublicationGet]
    }

    extend type Mutation{
        createPublication(publication:publicationCreate!):Publication!
        updatePublication(publication:publicationUpdate!):String!
        deletePublication(publicationId:String!):String!
    }

`;


module.exports = publicationsTypeDefs;
