const { gql } = require('apollo-server');

const UsersTypeDefs = gql `
    type Tokens {
        refresh: String!
        access: String!
    }

    type Access {
        access: String!
    }

    input CredentialsInput {
        email: String!
        password: String!
    }

    input updateSocialNetwork {
        id: Int!
        user_name: String!
        email: String!
        password: String!
        full_name: String!
        facebook: String
        instagram: String
        youtube: String
        twitter: String
    }

    input SignUpInput {
        user_name: String!
        email: String!
        password: String!
        full_name: String!
    }

    type UserDetail {
        id: Int!
        user_name: String!
        email: String!
        password: String!
        full_name: String!
        facebook: String
        twitter: String
        instagram: String
        youtube: String
    }

    type Mutation {
        signUpUser(userInput: SignUpInput): Tokens!
        updateSocial(user: updateSocialNetwork): String!
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(token: String!): Access!     
    }
    
    type Query {
        userDetailById(userId: Int!): UserDetail!
    }
`;

module.exports = UsersTypeDefs;
