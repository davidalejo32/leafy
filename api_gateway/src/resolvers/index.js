const userResolver        = require('./users_resolver');
const publicationResolver = require('./publications_resolver');
const commentsResolver    = require('./comments_resolver');
const lodash              = require('lodash')

const resolvers = lodash.merge(userResolver, publicationResolver, commentsResolver);
module.exports  = resolvers;

