const userResolver = {
   Query: {
      userDetailById: (_, { userId }, { dataSources, userIdToken }) => {
         if (userId == userIdToken) return dataSources.users_api.getUser(userId);
         else return null;
      },
   },

   Mutation: {
      signUpUser: async (_, { userInput }, { dataSources }) => {
         const user = {
            user_name: userInput.user_name,
            email: userInput.email,
            password: userInput.password,
            full_name: userInput.full_name,
         };
         return await dataSources.users_api.createUSer(user);
      },

      logIn: (_, { credentials }, { dataSources }) => 
         dataSources.users_api.loginUser(credentials),

      updateSocial: async (_, { user }, { dataSources }, { userIdToken }) => {
         usernameToken = dataSources.users_api.getUser(userIdToken).user_name;
         if (user == usernameToken) return await dataSources.users_api.updateUser(user);
         else return null;
      },

      refreshToken: async (_, { token }, { dataSources }) => {
         dataSources.users_api.refreshToken(token);
      },
   },
};

module.exports = userResolver;
