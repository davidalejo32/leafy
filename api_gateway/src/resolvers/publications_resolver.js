const publicationsResolver = {
   Query: {
      publications: async (_) => {
         return await dataSources.publications_api.getPublications();
      },

      publicationByUserName: async(_, { user_name }, { dataSources, userIdToken }) => {
         usernameToken = (await dataSources.users_api.getUser(userIdToken)).user_name

         if (user_name == usernameToken) {
            return await dataSources.publications_api.getPublicationByUsername(user_name)
         } else
            return null;
      }
   },

   Mutation: {
      createPublication: async (_, { publication }, { dataSources }, { userIdToken }) => {
         usernameToken = (dataSources.users_api.getUser(userIdToken)).user_name

         if (userIdToken == usernameToken)
            return await dataSources.publications_api.createPublication(publication)
         else
            return null
      },

      updatePublication: async (_, { publication }, { dataSources }, { userIdToken }) => {
         usernameToken = (dataSources.users_api.getUser(userIdToken)).user_name

         if (userIdToken == usernameToken)
            return await dataSources.publications_api.updatePublication(publication)
         else
            return null
      },

      deletePublication: async (_, { publicationId }, { dataSources }, { userIdToken }) => {
         usernameToken = (dataSources.users_api.getUser(userIdToken)).user_name

         if (userIdToken == usernameToken)
            return await dataSources.publications_api.removePublication(publicationId)
         else
            return null
      }

   }

};

module.exports = publicationsResolver;