const commnetsResolver = {
    Query:{
        comments:async(_)=>{
            return await dataSources.publications_api.getComments();
        },

        commentByPublicationId:async(_,{idPublication},{ dataSources, userIdToken})=>{
            
            usernameToken = (await dataSources.users_api.getUser(userIdToken)).user_name

            if(user_name == usernameToken){
                return (await dataSources.comments_api.getCommentsByIdPublications(idPublication),usernameToken);
            }else
                return null
        },
    },

    Mutation:{

        createComment:async(_,{comment},{ dataSources, userIdToken})=>{    
            usernameToken = (dataSources.users_api.getUser(userIdToken)).user_name     
            if(user_name == usernameToken){
                return await dataSources.comments_api.createComment(comment);
            }else
                return null
        },

        updatePublication:async(_,{comment},{ dataSources, userIdToken})=>{       
            usernameToken = (await dataSources.users_api.getUser(userIdToken)).user_name
            if(user_name == usernameToken){
                return await dataSources.comments_api.updateComment(comment);
            }else
                return null
        },

        deletePublication:async(_,{commentId},{ dataSources, userIdToken})=>{       
            usernameToken = (await dataSources.users_api.getUser(userIdToken)).user_name
            if(user_name == usernameToken){
                return await dataSources.comments_api.removeComment(commentId);
            }else
                return null
        }

    }

}

module.exports = commnetsResolver;