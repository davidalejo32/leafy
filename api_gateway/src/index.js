const { ApolloServer } = require('apollo-server');

const typeDefs  = require('./typeDefs');
const resolvers = require('./resolvers');
const users     = require('./utils/users');

const UsersApi        = require('./dataSources/users_api');
const PublicationsAPI = require('./dataSources/publications_api');
const CommentsAPI     = require('./dataSources/comments_api');


const server = new ApolloServer({
    context: users,
    typeDefs,
    resolvers,
    dataSources:() => ({
        usersAPI         : new UsersApi(),
        publicationsAPI  : new PublicationsAPI(),
        commentsAPI      : new CommentsAPI(),
    }),
    introspection: true,
    playground   : true
});

server.listen( process.env.PORT || 4000).then(({url}) =>{
        console.log(`🚀 Server ready at ${url}`);
    }
);