const { RESTDataSource } = require ("apollo-datasource-rest");
const serverConfig = require("../server")

class CommentsAPI extends RESTDataSource{
    constructor(){
        super();
        this.baseUrl = serverConfig.comments_api_url;
    }

    // get comments all
    async getComments(){
        return await this.get(`/comments/`);
    }

    // get commenst by idPublication
    async getCommentsByIdPublications(idPublication){
        return await this.get(`/comment/${idPublication}`);
    }

    //create comments
    async createComment(comment){
        comment = new Object(JSON.parse(JSON.stringify(comment)));
        return await this.post(`/comment/create/`, comment);
    }

    //update comments
    async updateComment(comment){
        comment = new Object(JSON.parse(JSON.stringify(comment)))
        let comentId = comment.id;
        return await this.put(`/comment/update/${comentId}/`, comment)
    }

    //remove comments
    async removeComment(commentId){
        return await this.delete(`/comment/remove/${commentId}/`)
    }
}

module.exports = CommentsAPI