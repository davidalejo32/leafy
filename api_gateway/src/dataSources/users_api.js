const { RESTDataSource } = require ('apollo-datasource-rest');
const serverConfig = require('../server');

class UsersApi extends RESTDataSource{

    constructor(){
        super();
        this.baseUrl = serverConfig.users_api_url;
    }

    //get User
    async getUser (userId){
        return await this.get(`/user/${userId}/`);
    }

    //user create
    async createUSer (user){
        user = new Object(JSON.parse(JSON.stringify(user)));
        return await this.post(`/user/`, user);
    }

    //login
    async loginUser(credentials) {
        credentials = new Object(JSON.parse(JSON.stringify(credentials)));
        return await this.post(`/login/`, credentials);
    }

    //refreshToken
    async refreshToken (token){
        token = new Object(JSON.parse(JSON.stringify(token)));
        return await this.post(`/refresh/`, token);
    }

    //user update
    async updateUser (user){
        user = new Object(JSON.parse(JSON.stringify(user)));
        let userId = user.id;
        return await this.put(`/user/update/${userId}/`, user);
    }

}

module.exports = UsersApi;