const { RESTDataSource } = require ("apollo-datasource-rest");
const serverConfig = require("../server")

class PublicationsAPI extends RESTDataSource{
    constructor(){
        super();
        this.baseUrl = serverConfig.publications_api_url;
    }

    // get publications all
    async getPublications(){
        return await this.get(`/publications/`);
    }

    // get publications by username
    async getPublicationByUsername(userName){
        return await this.get(`/publication/${userName}`);
    }

    // create publication  /publication/create/
    async createPublication (publication){
        publication = new Object(JSON.parse(JSON.stringify(publication)));
        return await this.post(`/publication/create/`, publication);
    }

    // update publication  publication/update/
    async updatePublication(publication){
        publication = new Object(JSON.parse(JSON.stringify(publication)));
        let publicationId = publication.id;
        return await this.update(`/publication/update/${publicationId}/`, publication);
    }

    // delete publication /publication/delete/
    async removePublication(publicationId){
        return await this.delete(`/publication/remove/${publicationId}/`);
    }
}

module.exports = PublicationsAPI;