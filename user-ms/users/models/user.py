from django.db import models
from django.contrib.auth.hashers import make_password

#clases basica de django de usuarios
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager


class UserProfileManager(BaseUserManager):
    ''' Manager para perfiles de usuario '''
    
    def create_user(self, email, user_name, full_name, password=None):
        ''' Crear nuevo User Profile '''
        if not email:
            raise ValueError('Usuario debe tener un Email')

        email = self.normalize_email(email)
        user  = self.model(email=email, user_name=user_name, full_name=full_name)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, user_name, full_name, password):
        ''' crear nuevo super usuario'''
        user = self.create_user(email, user_name, full_name, password,) 
        
        user.is_superuser = True
        user.is_staff     = True
        user.save(using=self._db)
        return user
        


class UserProfile(AbstractBaseUser, PermissionsMixin):
    ''' Modelo base de datos para usuarios en el Sistema '''
    id         = models.BigAutoField(primary_key=True) 
    user_name  = models.CharField("username", max_length=255)
    email      = models.EmailField('Email', max_length=255, unique=True)
    password   = models.CharField  ('Password', max_length = 256)
    full_name  = models.CharField('Fullname', max_length=255)
    is_staff   = models.BooleanField(default=False)
    facebook   = models.URLField('Facebokk', blank=True)
    twitter    = models.URLField('twitter', blank=True)
    instagram  = models.URLField('Instagram', blank=True)
    youtube    = models.URLField('youtube', blank=True)
    
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)
    
    
    objects = UserProfileManager()

    USERNAME_FIELD  = 'email'
    REQUIRED_FIELDS = ['user_name', 'full_name',]

    



















