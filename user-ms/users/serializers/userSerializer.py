from rest_framework     import serializers
from users.models.user import UserProfile

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model  = UserProfile
        fields = ['id','user_name', 'password', 'email', 'full_name', 'facebook', 'twitter', 'instagram', 'youtube']
        
    """
    def create(self, validated_data):
        userInstance = UserProfile.objects.create(**validated_data)
        return userInstance
    
    def to_representation(self, obj):
        user    = UserProfile.objects.get(id=obj.id)
        return {
            'user_name' : user.user_name,
            'email'     : user.email,
            'full_name' : user.full_name,
            'facebook'  : user.facebook,
            'twitter'   : user.twitter,
            'instagram' : user.instagram,
            'youtube'   : user.youtube, 
        }
    """