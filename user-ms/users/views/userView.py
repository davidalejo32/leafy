from rest_framework                       import status, views, generics
from rest_framework.response              import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from users.models.user                   import UserProfile
from users.serializers.userSerializer    import UserSerializer


class UserCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        tokenData = {"email":request.data["email"],
                     "password":request.data["password"]}
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)

        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)


class UserDetailView(generics.RetrieveAPIView): 
    queryset           = UserProfile.objects.all()
    serializer_class   = UserSerializer



class UserUpdateView(generics.UpdateAPIView):
    """
    Actualiza la publicacion por pk   
    """
    queryset           = UserProfile.objects.all()
    serializer_class   = UserSerializer
    
    def put(self, request, *args, **kwargs):
        try:
            put = super().update(request, *args, **kwargs)
            return Response(f"soical networks had been updated successfully")

        except Exception as e:
            return Response(f"Error: {e}", status=status.HTTP_400_BAD_REQUEST)

