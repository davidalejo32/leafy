from django.contrib    import admin
from users.models.user import UserProfile

admin.site.register(UserProfile)
