from django.urls                    import path
from users                          import views as userViews 
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)


urlpatterns = [
    path('login/',                 TokenObtainPairView.as_view()),
    path('refresh/',               TokenRefreshView.as_view()),
    path('user/',                  userViews.UserCreateView.as_view()),
    path('user/<int:pk>/',         userViews.UserDetailView.as_view()),
    path('user/update/<int:pk>/',  userViews.UserUpdateView.as_view()),
    path('verifyToken/',           userViews.VerifyTokenView.as_view()),
]