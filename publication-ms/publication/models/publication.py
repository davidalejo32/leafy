from django.db import models

class Publication(models.Model):
    id             = models.BigAutoField(primary_key=True)  
    user_name      = models.CharField('UserName', max_length=255, blank=False)
    date           = models.DateTimeField(auto_now=True)
    description    = models.TextField(blank=False)
    image          = models.TextField(blank=True)