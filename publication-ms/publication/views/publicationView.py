from rest_framework                                 import generics, status
from rest_framework.response                        import Response
#model
from publication.models.publication                 import Publication
#serializer
from publication.serializers.publicationSerializer  import PublicationSerializer



class Publications(generics.ListCreateAPIView):
    """
    muestra todas las publicaciones que realizaron todos los usuarios
    """
    queryset         = Publication.objects.all()
    serializer_class = PublicationSerializer 

 


class PublicationCreateView(generics.CreateAPIView):
    """
    crea una publicacion 
    """
    queryset           = Publication.objects.all()
    serializer_class   = PublicationSerializer
    
    def post(self, request, *args, **kwargs):
        
        serializer = PublicationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
       
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PublicationUpdateView(generics.UpdateAPIView):
    """
    Actualiza la publicacion por pk   
    """
    queryset           = Publication.objects.all()
    serializer_class   = PublicationSerializer
    
    def put(self, request, *args, **kwargs):
        try:
            put = super().update(request, *args, **kwargs)
            return Response(f"Publicacion Actualizada")

        except Exception as e:
            return Response(f"Error: {e}", status=status.HTTP_400_BAD_REQUEST)


class PublicationDeleteView(generics.DestroyAPIView):
    """
    borra la publicacion por pk   
    """
    queryset           = Publication.objects.all()
    Serializer_class   = PublicationSerializer
    
    def delete(self, request, *args, **kwargs):
        try:
            delete = super().destroy(request, *args, **kwargs)
            return Response(f"Publicacion Borrada")
        except Exception as e:
            return Response(f"Error: {e}", status=status.HTTP_400_BAD_REQUEST)


    



 