from rest_framework                  import serializers
from publication.models.publication  import Publication


class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Publication
        fields = ['id','user_name','date','description','image']
        
       
    