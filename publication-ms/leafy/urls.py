from django.contrib   import admin
from django.urls      import path
from publication      import views as publicationView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('publication/create/',              publicationView.PublicationCreateView.as_view()),
    path('publications/',                    publicationView.Publications.as_view()),
    path('publication/<str:user_name>',      publicationView.Publications.as_view()),
    path('publication/update/<int:pk>/',     publicationView.PublicationUpdateView.as_view()), 
    path('publication/remove/<int:pk>/',     publicationView.PublicationDeleteView.as_view()), 
]
