from django.db.models.query import QuerySet
from rest_framework import generics, status
from rest_framework.response import Response
from comments.models.Comments import Comment
from comments.serializers.commentSerializer import CommentSerializer

class Comments(generics.ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

class CommentCreatView(generics.CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def post(self, request, *arg, **kwargs):
        serializer = CommentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

class CommentUpdateView(generics.UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def put(self, request, *args, **kwargs):
        try:
            put=super().update(request, *args, **kwargs)
            return Response(f'Commentario Actualizado')
        
        except Exception as e:
            return Response(f'Error; {e}', status=status.HTTP_400_BAD_REQUEST)

class CommentdeleteView(generics.DestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def delete(self, request, *args, **kwargs):
        try:
            delete=super().destroy(request, *args, **kwargs)
            return Response(f'Commentario Eleminado')
        
        except Exception as e:
            return Response(f'Error; {e}', status=status.HTTP_400_BAD_REQUEST)
            