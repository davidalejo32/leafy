from rest_framework import serializers
from comments.models.Comments import Comment

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id','user_name','id_publication','content','date']