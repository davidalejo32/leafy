from django.db import models

class Comment(models.Model):
    id               = models.BigAutoField(primary_key=True)
    user_name        = models.CharField('UserName',max_length= 255, blank=False)
    id_publication   = models.IntegerField('Publication',blank= False) 
    content          = models.TextField(blank=False)
    date             = models.DateField(auto_now=True)


