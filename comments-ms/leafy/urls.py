from django.contrib     import admin
from django.urls        import path
from comments           import views as commentView

urlpatterns = [
    path('admin/',                       admin.site.urls),
    path('comments/',                    commentView.Comments.as_view()),
    path('comment/<int:id_publication>', commentView.Comments.as_view()),
    path('comment/update/<int:pk>/',     commentView.CommentUpdateView.as_view()),
    path('comment/create/',              commentView.CommentCreatView.as_view()),
    path('comment/remove/<int:pk>/',     commentView.CommentdeleteView.as_view()),
    
]
